import { Routes } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';
import { BlankComponent } from './components/blank/blank.component';
import { LoginComponent } from './components/login/login.component';

export const routes: Routes = [
  {
    path:"login",
    component:LoginComponent,
  },
  {
    path:"",
    component:LayoutComponent,
    children:[
      {
        path:"",
        component:BlankComponent
      }
    ]
  }
];
